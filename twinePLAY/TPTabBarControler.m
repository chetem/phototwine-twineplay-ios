//
//  TPTabBarController.m
//  twinePLAY
//
//  Created by Chris Hetem on 2/10/14.
//  Copyright (c) 2014 Chris Hetem. All rights reserved.
//

#import "TPTabBarController.h"
#import "TPGalleryViewController.h"
#import "TPCameraViewController.h"
#import "TPSettingsViewController.h"

@interface TPTabBarController ()

@property (nonatomic, strong) UINavigationController *galleryNavigationController;
@property (nonatomic, strong) UINavigationController *cameraNavigationController;
@property (nonatomic, strong) UINavigationController *settingsNavigationController;

@end


@implementation TPTabBarController


#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}


#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	// Events tab
	TPGalleryViewController *galleryTab = [[TPGalleryViewController alloc] init];
	galleryTab.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Gallery" image:[UIImage imageNamed:@"gallery-tab-inactive"]
												  selectedImage:[UIImage imageNamed:@"gallery-tab-active"]];
	[galleryTab.tabBarItem setTag:1];
	[galleryTab.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0, 0)];
	self.galleryNavigationController = [[UINavigationController alloc] initWithRootViewController:galleryTab];
	
	// Join tab
	TPCameraViewController *cameraTab = [[TPCameraViewController alloc] init];
	cameraTab.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Camera" image:[UIImage imageNamed:@"camera-tab-inactive"]
												 selectedImage:[UIImage imageNamed:@"camera-tab-active"]];
	[cameraTab.tabBarItem setTag:2];
	[cameraTab.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0, 0)];
	self.cameraNavigationController = [[UINavigationController alloc] initWithRootViewController:cameraTab];
	
	// Settings tab
	TPSettingsViewController *settingsTab = [[TPSettingsViewController alloc] init];
	settingsTab.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Settings" image:[UIImage imageNamed:@"settings-tab-inactive"]
												   selectedImage:[UIImage imageNamed:@"settings-tab-active"]];
	[settingsTab.tabBarItem setTag:3];
	[settingsTab.tabBarItem setTitlePositionAdjustment:UIOffsetMake(0, 0)];
	self.settingsNavigationController = [[UINavigationController alloc] initWithRootViewController:settingsTab];
	
	// Set the view controllers
	[self setViewControllers:@[self.galleryNavigationController, self.cameraNavigationController, self.settingsNavigationController]];
	[self setSelectedIndex:0];
	
	// Join button action
//	[self.joinButton addTarget:self action:@selector(onJoinTabSelected:) forControlEvents:UIControlEventTouchUpInside];
	
}


#pragma mark - Button Action

- (void)onJoinTabSelected:(id)sender
{
//	[self setSelectedIndex:1];
//	[self.joinButton setSelected:YES];
}

@end
