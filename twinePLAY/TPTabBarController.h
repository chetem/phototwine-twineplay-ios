//
//  TPTabBarController.h
//  twinePLAY
//
//  Created by Chris Hetem on 2/10/14.
//  Copyright (c) 2014 Chris Hetem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPTabBarController : UITabBarController <UINavigationControllerDelegate>

@end
