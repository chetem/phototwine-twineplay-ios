//
//  TPAppDelegate.m
//  twinePLAY
//
//  Created by Chris Hetem on 2/10/14.
//  Copyright (c) 2014 Chris Hetem. All rights reserved.
//

#import "TPAppDelegate.h"
#import "TPLoginViewController.h"

@implementation TPAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	//create overall app appearance
	
	//make sure status bar is displayed
	[[UIApplication sharedApplication]setStatusBarHidden:NO];
	[[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
	
	//set nav bar tint color to green value
	[[UINavigationBar appearance]setBarTintColor:[UIColor colorWithRed:0.0f/255.0f green:102.0f/255.0f blue:51.0f/255.0f alpha:1.0]];
	
	
	//set tab bar text color to green value
	[[UITabBar appearance] setTintColor:[UIColor colorWithRed:0.0f/255.0f green:102.0f/255.0f blue:51.0f/255.0f alpha:1.0]];
	
	// Navigation bar appearance to have white text
	NSDictionary *titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
										[UIFont fontWithName:@"HelveticaNeue-Bold" size:20.0f], NSFontAttributeName,
										[UIColor whiteColor], NSForegroundColorAttributeName,
										nil];
	
	[[UINavigationBar appearance] setTitleTextAttributes:titleTextAttributes];
	
	[[UINavigationBar appearance]setTintColor:[UIColor whiteColor]];
	
	//Tab bar text appearance 
	NSDictionary *barButtonItemAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
											 [UIFont fontWithName:@"HelveticaNeue" size:17.0f], NSFontAttributeName,
											 [UIColor whiteColor], NSForegroundColorAttributeName,
											nil];
	
	[[UIBarButtonItem appearance]setTitleTextAttributes:barButtonItemAttributes forState:UIControlStateNormal];

	
	//create view hierarchy
	
	// Create the navigation controller
	TPTabBarController *vc = [[TPTabBarController alloc] init];
	
	// Create the tabblar controller
	self.tabbarController = [[TPTabBarController alloc] init];
	self.tabbarController.delegate = self;
	
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
	self.window.rootViewController = vc;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
