//
//  TPAppDelegate.h
//  twinePLAY
//
//  Created by Chris Hetem on 2/10/14.
//  Copyright (c) 2014 Chris Hetem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPTabBarController.h"

@interface TPAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) TPTabBarController *tabbarController;

@end
