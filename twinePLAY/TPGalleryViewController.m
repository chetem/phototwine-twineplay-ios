//
//  TPGalleryViewController.m
//  twinePLAY
//
//  Created by Chris Hetem on 2/10/14.
//  Copyright (c) 2014 Chris Hetem. All rights reserved.
//

#import "TPGalleryViewController.h"
#import "TPGalleryCell.h"

@interface TPGalleryViewController ()

@end

@implementation TPGalleryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	self.title = @"Gallery";
	
	
	UIBarButtonItem *shareButton =[[UIBarButtonItem alloc]
								   initWithBarButtonSystemItem:UIBarButtonSystemItemAction
														target:self
														action:@selector(onShare)];
	self.navigationItem.rightBarButtonItem = shareButton;
}

#pragma  mark - Button Actions

-(void)onShare
{
	DLog(@"Share button pressed");
}

#pragma mark - TableView Delegate/Datasource

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"CellIdentifier";
	
	// Init cell and register the nib file
	UINib *cellNib = [UINib nibWithNibName:@"TPGalleryCell" bundle:nil];
	[self.galleryTableView registerNib:cellNib forCellReuseIdentifier:cellIdentifier];
		
	TPGalleryCell *cell = (TPGalleryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
	
	
	return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 3;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 300.0f;
}

@end
