//
//  TPSettingsViewController.m
//  twinePLAY
//
//  Created by Chris Hetem on 2/10/14.
//  Copyright (c) 2014 Chris Hetem. All rights reserved.
//

#import "TPSettingsViewController.h"

@interface TPSettingsViewController ()

@end

@implementation TPSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	self.title = @"Settings";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
