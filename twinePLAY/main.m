//
//  main.m
//  twinePLAY
//
//  Created by Chris Hetem on 2/10/14.
//  Copyright (c) 2014 Chris Hetem. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TPAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([TPAppDelegate class]));
	}
}
